# Docker Images for Bitbucket Pipelines

## ubuntu-16.04

`redisca/bitbucket-pipelines:ubuntu-16.04`

Environment:

 + python 3.5 (with pre-built psycopg2, pillow and lxml)
 + build-essential (gcc, make, etc...)
 + postgresql 9.5
 + nodejs 6.4
 + openssh
 + curl
 + rsync
 + fabric
 + ansible 2.0

## Quick Start

```
image: redisca/bitbucket-pipelines:ubuntu-16.04

pipelines:
  branches:
    default:
      - step:
          script:
            - grep -iv -e 'pillow' -e 'psycopg2' -e 'lxml' requirements.txt > requirements-pipelines.txt
            - pip3 install -r requirements-pipelines.txt
            - service postgresql start
            - DJANGO_SETTINGS_MODULE=settings.test python3 manage.py test
            - ansible-playbook -i stage.ini playbooks/deploy.yml
```
