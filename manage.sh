#!/bin/bash

set -e

BASE_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)
IMAGE="redisca/bitbucket-pipelines"

build() {
    version=$1
    docker build -t "$IMAGE:$version" "$BASE_DIR/$version"
}

push() {
    version=$1
    docker push "$IMAGE:$version"
}

main() {
    command=$1
    shift

    ${command} $@
    if [[ $? = 127 ]]; then
        echo "Error: '$command' is not a known subcommand." >&2
        exit 1
    fi
}

main $@
